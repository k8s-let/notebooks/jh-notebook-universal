FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-base-ethz:3.0.0-16

USER root

# Julia
RUN http_proxy=http://proxy.ethz.ch:3128 https_proxy=http://proxy.ethz.ch:3128 julia -e "using Pkg; Pkg.add.([ \
	\"NBInclude\", \ 
	\"PyPlot\", \ 
	\"Roots\", \
	\"UnPack\", \ 
        \"WhereTheWaterFlows\", \
        \"BenchmarkTools\", \
	\"Plots\" \
]); Pkg.update;"

# R libs
#
# RcppEigen generates huge amounts of output. Install first and mute this step
RUN Rscript >/dev/null 2>&1 -e "install.packages(pkgs=c('RcppEigen','sensitivity','blockmodeling','GGally','ggforce','ggraph','igraph','matlib','nloptr','RBGL','RColorBrewer','sbm','sna','tidygraph'), repos=c('http://cran.r-project.org'), dependencies=TRUE, timeout=300)" \
    && \
    Rscript -e "install.packages(pkgs=c('BiocManager'), repos=c('http://cran.r-project.org'))" \
    && \
    Rscript -e "BiocManager::install()" \
    && \
    Rscript -e "devtools::install_github(repo = 'AECP-ETHZ/ETH.OLP')"

# Python and R from Conda
#
# traitlets for rise
RUN pip3 uninstall  -y traitlets
RUN mamba install -q -c defaults -c conda-forge -c bioconda -c bokeh \
  arviz \
  autogluon \
  biopython \
  bokeh \
  boost-histogram \
  corner \
  dynesty \
  emcee \
  ete3 \
  flax \
  geopandas \
  graph-tool \
  igraph \
  jupyterlab-vpython \
  keras \
  meshio \
  mplcursors \
  multi_rake \
  nbgitpuller \
  optax \
  pathos \
  pint \
  plotly \
  py3dmol \
  pylatex \
  pyteomics \
  pytesseract \
  pyspellchecker \
  rdkit \
  scikit-image \
  scikit-learn \
  seaborn \
  sparqlwrapper \
  tabulate \
  tinygp \
  uncertainties \
  wikipedia \
  wordcloud \
  yake \
  r-ape \
  r-bio3d \
  r-devtools \
  r-lpsolve \
  r-lhs \
  r-rsolnp \
  r-terra \
  && \
  mamba clean --all \
  && \
  jupyter labextension install jupyterlab_3dmol
  # To correctly display py3Dmol in Jupyter

# Packages not available for conda/mamba, or outdated
RUN pip3 uninstall -y jaxlib
RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
  asserts \
  brainpy \
  cobra \
  deep_translator \
  einsteinpy \
  gmsh \
  jax>=0.4.21 \
  jaxlib>=0.4.21 \
  jaxopt \
  jupyter_ai \
  jupyterlab-deck \
  mycolorpy \
  mpl_point_clicker \
  networkit \
  networkx \
  keybert \
  obspy \
  opencv-python opencv-contrib-python \
  otter-grader \
  probfit \
  pycosmo \
  pycryptodome \
  pyvista \
  "tensorflow-probability[jax]" \
  torch \
  umap-learn \
  rise \
  sensemapi \
  scikit-kinematics \
  summa \
  trame \
  trame-vuetify \
  vpython \
  wiktionaryparser

# RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
#   jupyterlab_myst

# User submitted support programs
COPY bin /usr/local/bin/

# Configuration items
COPY etc /etc

USER 1000
